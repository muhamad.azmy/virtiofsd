use std::fmt::Debug;
use std::fmt::Display;
use std::io::Write;
use std::io::{Error, ErrorKind, Read, Result};
use std::os::fd::FromRawFd;
use std::os::unix::net::UnixStream;
use std::process::Command;
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use std::thread;

#[derive(Debug, Default)]
pub struct StatFS {
    pub size: u64,
    pub available: u64,
}

impl Display for StatFS {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.size, self.available)
    }
}

// parse from string formatted as `1245 52345`
impl FromStr for StatFS {
    type Err = Error;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let parts: Vec<&str> = s.trim().split(' ').collect();
        if parts.len() != 2 {
            return Err(Error::new(
                ErrorKind::Other,
                "invalid output format expecting 2 numbers",
            ));
        }

        let size: u64 = parts[0]
            .parse()
            .map_err(|err| Error::new(ErrorKind::Other, err))?;
        let avail: u64 = parts[1]
            .parse()
            .map_err(|err| Error::new(ErrorKind::Other, err))?;

        Ok(Self {
            size,
            available: avail,
        })
    }
}

/// The StatFSCmd is implemented so that command execution happens outside the
/// sandbox and seccomp rules.
///
/// Once an instance is created a unix socket pair is created one of them is used
/// as a server. the other is used as a client.
///
/// When a client send a request (which is just one byte) the server thread receives that
/// and executes the command, and parses it's output. Then it sends the statfs back over
/// the wire to
pub struct StatFSCmd {
    cl: Arc<Mutex<UnixStream>>,
}

impl Debug for StatFSCmd {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "statfs command")
    }
}

fn socketpair() -> Result<(UnixStream, UnixStream)> {
    let mut fds: [libc::c_int; 2] = [0; 2];

    let (p1, p2) = unsafe {
        let output = libc::socketpair(
            libc::AF_UNIX,
            libc::SOCK_STREAM,
            0,
            &mut fds as *mut libc::c_int,
        );

        if output != 0 {
            return Err(Error::last_os_error());
        }

        let p1 = UnixStream::from_raw_fd(fds[0]);
        let p2 = UnixStream::from_raw_fd(fds[1]);

        (p1, p2)
    };

    Ok((p1, p2))
}

impl StatFSCmd {
    pub fn new<S: AsRef<str>>(cmd: S) -> Result<Self> {
        let parts = shlex::split(cmd.as_ref())
            .ok_or_else(|| Error::new(ErrorKind::Other, "invalid statfs command"))?;

        if parts.is_empty() {
            return Err(Error::new(ErrorKind::Other, "invalid empty command"));
        }

        let (client, mut server) = socketpair()?;

        thread::spawn(move || {
            let mut buf: [u8; 1] = [0; 1];

            while server.read_exact(&mut buf).is_ok() {
                // we only receive one byte assume we have
                // a request. Not much to ask

                // run the command and build response
                let output =
                    Command::new(&parts[0])
                        .args(&parts[1..])
                        .output()
                        .and_then(|output| {
                            if !output.status.success() {
                                error!("statfs: {}", String::from_utf8_lossy(&output.stderr));
                                return Err(Error::new(
                                    ErrorKind::Other,
                                    "statsfs command existed with error",
                                ));
                            }

                            let stat: StatFS = String::from_utf8_lossy(&output.stdout).parse()?;
                            Ok(stat)
                        });

                // to simplify wire protocol and avoid
                // using or implementing a serialization
                // protocol. send default statfs if error
                // occurred. But log it so it's visible
                // to user
                let output = match output {
                    Ok(output) => output,
                    Err(err) => {
                        error!("statfs error: {}", err);
                        StatFS::default()
                    }
                };

                let data = output.to_string();
                // write response back
                // first we send the length of the output in one byte
                if let Err(err) = server.write(&[data.as_bytes().len() as u8]) {
                    error!("failed to return response length: {:#}", err);
                    continue;
                }

                // then followed by the response
                if let Err(err) = server.write_all(data.as_bytes()) {
                    error!("failed to return response: {:#}", err)
                }

                _ = server.flush();
            }
            debug!("stat loop exited");
        });

        Ok(Self {
            cl: Arc::new(Mutex::new(client)),
        })
    }

    // stat returns a statfs object.
    pub fn stat(&self) -> Result<StatFS> {
        // todo: maybe not safe to panic when acquiring the lock
        let mut client = self.cl.lock().unwrap();

        // buffer is enough to hold 2xU64 MAX values separated by a single space
        let mut buf: [u8; 50] = [0; 50];
        // we first send a "request" which is a single byte
        // the server side will receive this, and run the command
        // outside of both the sandbox and seccomp rules
        client.write_all(&buf[0..1])?;

        // read response length from command
        // it's one byte long
        client.read_exact(&mut buf[0..1])?;
        let len = buf[0] as usize;

        if len > buf.len() {
            return Err(Error::new(ErrorKind::Other, "invalid response length"));
        }

        // then read the full response
        client.read_exact(&mut buf[0..len])?;

        // parse the output
        let statfs: StatFS = String::from_utf8_lossy(&buf[0..len]).parse()?;

        if statfs.size == 0 {
            // we make the protocol simple for this we just
            // return default statfs on error.
            return Err(Error::new(
                ErrorKind::Other,
                "invalid command output. please check logs",
            ));
        }

        Ok(statfs)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_statfs_cmd() {
        let stat_cmd = StatFSCmd::new("echo 1234 567").unwrap();

        let stat = stat_cmd.stat().unwrap();
        assert_eq!(1234, stat.size);
        assert_eq!(567, stat.available);

        let stat = stat_cmd.stat().unwrap();
        assert_eq!(1234, stat.size);
        assert_eq!(567, stat.available);
    }

    #[test]
    fn test_statfs_bad() {
        let stat_cmd = StatFSCmd::new("echo abcd efg").unwrap();

        let stat = stat_cmd.stat();

        assert!(
            matches!(stat, Err(err) if err.kind() == ErrorKind::Other && err.to_string() == "invalid command output. please check logs")
        );
    }
}
